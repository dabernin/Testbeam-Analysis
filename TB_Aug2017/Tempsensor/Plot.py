from __future__ import print_function

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

DF = pd.read_csv("temperatureLog.txt", delim_whitespace=True,
                 names=["Sensor", "Date", "Time", "Temperature"],
                 parse_dates=[["Date", "Time"]], dayfirst=True, infer_datetime_format=True)

DF["Temperature"] = pd.to_numeric(DF["Temperature"], errors="coerce")
DF = DF.dropna()
DF = DF[ DF["Temperature"] > 0 ]

plt.figure(figsize=(20, 10))
for sensor, sensor_data in DF.groupby("Sensor"):
    plt.plot_date(sensor_data["Date_Time"], sensor_data["Temperature"], "H", label=sensor, ls="-",
                  markersize=0)

plt.setp(plt.gca().xaxis.get_majorticklabels(),
         'rotation', 90)
plt.xlabel("Time")
plt.ylabel("Temperature [$\circ$C]")
plt.legend()

plt.savefig("Temperature_vs_time.pdf", bbox_inches="tight")
