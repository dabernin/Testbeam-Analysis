from __future__ import division

import numpy as np

class Cluster(object):
    """A Cluster."""
    def __init__(self, chan_ID, seed_th):
        #A Cluster is created from a channel exceeding the seed threshold
        self.Channels = [ chan_ID ]                #List with all channels involved in the cluster
        self.Thresholds = [ seed_th ]              #List with thresholds corresponding to entry in Channels

    def __repr__(self):
        return "CLUSTER: Chs,Ths {}".format(zip(self.Channels, self.Thresholds))

    def __eq__(self, other):
        if( (self.Channels == other.Channels) and (self.Thresholds == other.Thresholds) ):
            return True
        else:
            return False

    def add(self, chan_ID, th):
        #Add a channel with a corresponding threshold to the cluster
        self.Channels.append(chan_ID)
        self.Thresholds.append(th)

    def Position(self, weights=[0,2,4,12]):
        #Return the weighted position of the cluster
        #Weights determines how the channel should be weighted depending on
        #the exceeded threshold (can be 0,1,2,3)
        if len(weights) != 4:
            raise ValueError("You must provide 4 weights! ({} given)".format(len(weights)))

        chweights = np.asarray([ weights[th] for th in self.Thresholds ])

        return float(np.sum( np.asarray(self.Channels) * chweights )) / np.sum(chweights)

    def ThMax(self):
        #Return the maximum threshold
        return max(self.Thresholds)

    def Size(self):
        #Return the size of the cluster
        return len(self.Channels)

    def Range(self):
        #Return the range, i.e. min and max channel_ID
        return [min(self.Channels), max(self.Channels)]

    def Plot(self, histtype="step", color="gcblue", alpha=0.25, fill=True, *args, **kwargs):
        #Plot the cluster (as histogram with channels on x-axis and exceeded thresholds on y)
        #args and kwargs are forwarded to matplotlibs hist function
        dpl.hist( np.asarray(self.Channels), weights=self.Thresholds, histtype=histtype, color=color, alpha=alpha,
                  bins=np.arange(self.Range()[0]-0.5, self.Range()[1]+1.5, 1), fill=fill, *args, **kwargs )




def cluster_algorithm(channel_arr, channel_range=None, seed_th=2, min_size=2, single_ch_th=3,
                     continue_th=None):
    """Algorithm for generating clusters for one event. channel_arr is an array with length=n_channels.
    - channel_range are the numbers of channels corresponding to the given channel_arr.
      (Necessary for the determination of the weighted position)
    - seed_th is the threshold that must be exceeded for starting a cluster.
    - min_size a number indicating the minimum size of a cluster to actually be a cluster.
    - single_ch_th a number (1,2 or 3) for a cluster being a cluster if it does not exceed min_size but one channel
      exceeding the threshold indicated by this number.
    - continue_th is the threshold that must be exceeded to continue looking for further channels belonging to the
      cluster. Defaults to None which refers to the seed_th."""
    if not continue_th:
        continue_th = seed_th
    elif continue_th > seed_th:
        print("WARNING: continue threshold is larger than seed threshold!")


    Clusters = []   #List containing all clusters in this event

    #Sanity check
    n_channels = len(channel_arr)

    if channel_range is None:
        channel_range = range(len(channel_arr))
    if len(channel_range) != n_channels:
        raise ValueError("Length of channel_arr and channel_range must match!")

    #Loop through the channels
    chan_ID = 0
    while chan_ID < n_channels:
        #Seed found? => start potential cluster!
        if channel_arr[chan_ID] >= seed_th:
            pot_cluster = Cluster(channel_range[chan_ID], channel_arr[chan_ID])

            #Neighbouring channel(s) left of this channel?
            chan_ID_left = chan_ID-1
            while chan_ID_left >= 0 and channel_arr[chan_ID_left] >= 1:
                th = channel_arr[chan_ID_left]
                pot_cluster.add(channel_range[chan_ID_left], th)
                if th < continue_th:
                    break
                chan_ID_left -= 1

            #Go further right..
            chan_ID += 1
            while chan_ID < n_channels and channel_arr[chan_ID] >= 1:
                th = channel_arr[chan_ID]
                pot_cluster.add(channel_range[chan_ID], th)
                if th < continue_th:
                    break
                chan_ID += 1   #Th exceeds continue_th => keep looking!

            #Finished with potential cluster, is it an actual cluster?
            if pot_cluster.ThMax() >= single_ch_th or pot_cluster.Size() >= min_size:
                Clusters.append(pot_cluster)

        chan_ID += 1

    return Clusters
