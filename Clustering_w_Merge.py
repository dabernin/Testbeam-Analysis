from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm, Cluster
import numpy as np
import root_numpy as rn
import pandas as pd
from argparse import ArgumentParser
import TBFunctions as TB


#Channels to use for the clustering
channels = range(0, 64, 1)
SPIROC_channels = range(128, 192, 1)

#Channels to drop in the data for the SPIROC
SPIROC_keep_channels = ["Ch_{}".format(ch) for ch in range(128, 256)]

#Data_columns for on-disk queries
PACIFIC_data_columns = [ "Layer", "BXing", "Integrator", "Finetiming", "TrackHit_Ch",
                         "Track_Chi2NDOF", "TelescopeClusters", "TrackHit_Y" ]
SPIROC_data_columns = [ "Layer", "TrackHit_Ch", "Track_Chi2NDOF", "TelescopeClusters",
                        "TrackHit_Y" ]


#Possible cluster algorithm settings to choose from
cluster_algo_settings = { "Default" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3},
                          "No_requirements" : {"seed_th" : 2, "min_size" : 0, "single_ch_th" : 0},
                          "Seed_1" : {"seed_th" : 1, "min_size" : 0, "single_ch_th" : 0},
                          "Big" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3, "continue_th" : 1}
                        }

SPIROC_ThSettings = { 1 : [0.5, 1.5, 2.5],
                      2 : [1.5, 2.5, 3.5],
                      3 : [1.5, 2.5, 4.5]
                    }


def create_additional_rows(DF):
    #Create additional rows for max and sum of the exceeded thresholds
    #Prepare transformation dictionary
    groupkey = "Evt_num"
    BXkey = "BXing"
    Chpattern = "Ch_"
    columns = list(DF.columns)

    if not groupkey in columns:
        raise ValueError("Groupkey {} not in columns of dataframe!".format(groupkey))
    columns.remove(groupkey)
    if not BXkey in columns:
        raise ValueError("BXkey {} not in columns of dataframe!".format(BXkey))
    columns.remove(BXkey)

    Ch_columns, Other_columns = [], []
    for column in columns:
        if Chpattern in column:
            Ch_columns.append(column)
        else:
            Other_columns.append(column)

    add_DFs = []
    for new_BX in [-1, -2]:  #-1 is max, -2 is sum
        if new_BX == -1:
            func = "max"
            print("Calculating the maximum")
        elif new_BX == -2:
            func = "sum"
            print("Calculating the sum")
        aggregation_dict = {}
        aggregation_dict[BXkey] = lambda x: new_BX
        for Ch_column in Ch_columns:
            aggregation_dict[Ch_column] = func
        for Other_column in Other_columns:
            aggregation_dict[Other_column] = "max"  #Anyway the same

        print("Agg dictionary created:", aggregation_dict)

        DF_group = DF.groupby(groupkey, as_index=False).agg( aggregation_dict )

        if new_BX == -2:
            print("Limit sum to 3.")
            DF_group.loc[:, Ch_columns] = DF_group.loc[:, Ch_columns].clip(upper=3)

        #Force channel columns to int
        #DF_group.loc[:, Ch_columns] = DF_group.loc[:, Ch_columns].astype("int16")
        add_DFs.append( DF_group )

    print("Merging...")
    columns = DF.columns.tolist()  #For later reordering of columns
    for add_DF in add_DFs:
        DF = DF.append(add_DF)

    print("Sorting...")
    DF.sort_values([groupkey, BXkey], inplace=True)
    DF.reset_index(inplace=True)
    DF = DF.reindex_axis(columns, axis=1, copy=False)

    return DF


if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create clusters and merge into data")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-t", "--tree", dest="Tree", action="store", required=False,
                        choices=["PACIFIC", "SPIROC"], default="PACIFIC")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output HDF5-file")
    parser.add_argument( "-s", "--thsetting", dest="ThSetting", action="store", required=False,
                        type=int, choices=[1, 2, 3], help="Threshold setting (SPIROC only)")
    parser.add_argument( "-n", "--nevents", dest="Nevents", action="store", required=False,
                        type=int, help="Only process this amount of events.")


    #Parse arguments from command line
    options = parser.parse_args()

    #Check if -s is provided when -t is provided
    if options.Tree == "SPIROC" and not options.ThSetting:
        raise ValueError("Processing SPIROC data but not threshold setting (1, 2 or 3) was set!")



    #==============================
    # PACIFIC ======================
    #==============================
    if options.Tree == "PACIFIC":
        print("PACIFIC data provided")
        #Open root file
        print("Reading file {}..., Tree {}".format(options.Input, options.Tree))
        DF = pd.DataFrame( rn.root2array(options.Input, options.Tree) )
        print("Loaded into pandas DataFrame ({} rows).".format(len(DF)))

        if options.Nevents:
            print("Only taking {} events/rows for later processing".format(options.Nevents))
            DF = DF.iloc[:options.Nevents]

        unique_BXings = np.unique(DF["BXing"])
        print("Found BXings: ", unique_BXings)
        if len(unique_BXings) > 1:
            print("More than one BXing in DataFrame, create additional rows (sum, max).")
            DF = create_additional_rows(DF)


        data_columns = PACIFIC_data_columns
        cluster_channels = channels







    #==============================
    # SPIROC ======================
    #==============================
    if options.Tree == "SPIROC":
        print("SPIROC data provided")
        #Get columns from root-file
        all_columns = rn.list_branches(options.Input, options.Tree)
        columns = []
        for col in all_columns:  #Keep only specific Ch_ columns
            if "Ch_" not in col or col in SPIROC_keep_channels:
                columns.append(col)

        print("Read columns {} from {}, Tree {}".format(columns, options.Input, options.Tree))
        DF = pd.DataFrame( rn.root2array(options.Input, options.Tree, branches=columns) )
        print("Loaded into pandas DataFrame ({} rows).".format(len(DF)))

        if options.Nevents:
            print("Only taking {} events/rows for later processing".format(options.Nevents))
            DF = DF.iloc[:options.Nevents]

        #Transform SPIROC pixels into exceeded thresholds
        ThS = options.ThSetting
        print("Transforming channel pixels in thresholds ThS{} ({})".format(ThS,
                                                                            SPIROC_ThSettings[ThS]))
        DF[ SPIROC_keep_channels ] = TB.SPIROC2Ths( DF[ SPIROC_keep_channels ].values,
                                                    SPIROC_ThSettings[ThS] )


        data_columns = SPIROC_data_columns
        cluster_channels = SPIROC_channels




    #=========
    #COMMON
    #=========
    #Save to HDF
    print("Saving Data to {}/Data...".format(options.Output))
    DF.to_hdf(options.Output, "Data", format="table", mode="w", complevel=1, complib="zlib",
              data_columns=data_columns)

    print("Performing clustering")
    chmatrix = DF[ ["Ch_{}".format(ch) for ch in cluster_channels] ].values
    for name, settings in cluster_algo_settings.items():
        print(name, settings)
        DF_Clusters = pd.DataFrame(index=DF.index)
        key = "Clusters_{}".format(name)
        DF_Clusters[key] = [ cluster_algorithm( charray, channel_range=cluster_channels,
                                               **settings) for charray in chmatrix ]

        #Save to HDF (every cluster configuration in a different key (performance reasons))
        print("Saving Clusters to {}".format(key))
        DF_Clusters.to_hdf(options.Output, key, format="fixed", mode="a",
                           complevel=1, complib="zlib")
