from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm, Cluster
import numpy as np
import root_numpy as rn
import pandas as pd
from argparse import ArgumentParser
import TBFunctions as TB



def create_additional_rows(DF):
    #Create additional rows for max and sum of the exceeded thresholds
    #Prepare transformation dictionary
    groupkey = "Evt_num"
    BXkey = "BXing"
    Chpattern = "Ch_"
    columns = list(DF.columns)

    if not groupkey in columns:
        raise ValueError("Groupkey {} not in columns of dataframe!".format(groupkey))
    columns.remove(groupkey)
    if not BXkey in columns:
        raise ValueError("BXkey {} not in columns of dataframe!".format(BXkey))
    columns.remove(BXkey)

    Ch_columns, Other_columns = [], []
    for column in columns:
        if Chpattern in column:
            Ch_columns.append(column)
        else:
            Other_columns.append(column)

    add_DFs = []
    for new_BX in [-1, -2]:  #-1 is max, -2 is sum
        if new_BX == -1:
            func = "max"
            print("Calculating the maximum")
        elif new_BX == -2:
            func = "sum"
            print("Calculating the sum")
        aggregation_dict = {}
        aggregation_dict[BXkey] = lambda x: new_BX
        for Ch_column in Ch_columns:
            aggregation_dict[Ch_column] = func
        for Other_column in Other_columns:
            aggregation_dict[Other_column] = "max"  #Anyway the same

        print("Agg dictionary created:", aggregation_dict)

        DF_group = DF.groupby(groupkey, as_index=False).agg( aggregation_dict )

        if new_BX == -2:
            print("Limit sum to 3.")
            DF_group.loc[:, Ch_columns] = DF_group.loc[:, Ch_columns].clip(upper=3)

        #Force channel columns to int
        #DF_group.loc[:, Ch_columns] = DF_group.loc[:, Ch_columns].astype("int16")
        add_DFs.append( DF_group )

    print("Merging...")
    columns = DF.columns.tolist()  #For later reordering of columns
    for add_DF in add_DFs:
        DF = DF.append(add_DF)

    print("Sorting...")
    DF.sort_values([groupkey, BXkey], inplace=True)
    DF.reset_index(inplace=True)
    DF = DF.reindex_axis(columns, axis=1, copy=False)

    return DF


if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create additional BXs (-1: 'max' and -2: 'sum')")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output ROOT-file")
    parser.add_argument( "-n", "--nevents", dest="Nevents", action="store", required=False,
                        type=int, help="Only process this amount of events.")


    #Parse arguments from command line
    options = parser.parse_args()

    #Open root file
    print("Reading file {}..., Tree {}".format(options.Input, "PACIFIC"))
    if options.Nevents:
        nevents = options.Nevents
        print("Read only {} events/rows".format(nevents))
    else:
        nevents = None

    DF = pd.DataFrame( rn.root2array(options.Input, "PACIFIC", stop=nevents) )
    print("Loaded into pandas DataFrame ({} rows).".format(len(DF)))

    unique_BXings = np.unique(DF["BXing"])
    print("Found BXings: ", unique_BXings)
    if len(unique_BXings) > 1:
        print("More than one BXing in DataFrame, create additional rows (sum, max).")
        DF = create_additional_rows(DF)


    rn.array2root(DF.to_records(), options.Output, "PACIFIC", mode="recreate")
    print("Wrote to file {}".format(options.Output))
