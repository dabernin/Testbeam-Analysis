from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm
import numpy as np
import pandas as pd
import ROOT
from argparse import ArgumentParser

channels = range(64)

#Get value from event
def getCurrentValue(event, branchName):
     return getattr(event,branchName)


#Possible cluster algorithm settings to choose from
cluster_algo_settings = { "Default" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3},
                          "No_requirements" : {"seed_th" : 2, "min_size" : 0, "single_ch_th" : 0},
                          "Seed_1" : {"seed_th" : 1, "min_size" : 0, "single_ch_th" : 0},
                          "Big" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3, "continue_th" : 1}
                        }


if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create clusters and store them in a pickle file")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output Pickle-file")
    parser.add_argument( "-a", "--algorithm", dest="Algorithm", action="store", required=True,
                         help="Algorithm name to use")
    parser.add_argument( "-n", "--nrows", dest="Nrows", action="store", required=False, type=int,
                         help="Lines to process at least (last event will be fully processed), "
                         "optional", default=None )

    #Parse arguments from command line
    options = parser.parse_args()


    setting = cluster_algo_settings[options.Algorithm]

    if options.Nrows:
        print("-n set. Will stop after processing {} entries.".format(options.Nrows))

    #Open tree
    inputfile = ROOT.TFile(options.Input, "READ")
    if not inputfile.IsOpen():
        raise SystemExit("Could not open inputfile!")

    inputtree = inputfile.Get("PACIFIC")


    #Create list for evt_num, clusters tuple
    evtnum_bxing_clusters = []


    ch_array = np.zeros(len(channels), dtype=int)
    ch_array_max = np.zeros(len(channels), dtype=int)  #Max of all BXs
    ch_array_sum = np.zeros(len(channels), dtype=int)  #Sum of all BXs

    last_BXing = -1
    last_evt_num = -1
    #Loop through TTree
    breaked = False
    n_entries = inputtree.GetEntries()
    for i, event in enumerate(inputtree):
        if i % 10000 == 0:
            print("{} of {} entries processed ({:.2%})".format(i, n_entries, i/n_entries))
        evt_num = getCurrentValue(event,"Evt_num")
        BXing = getCurrentValue(event,"BXing")
        for idx, ch_no in enumerate(channels):
            ch_array[idx] = getCurrentValue(event,"Ch_{}".format(ch_no))

        if BXing < last_BXing:  #We are through a pack of bunch crossings
            if last_evt_num >= evt_num:
                raise ValueError("Current evt_num {} equal or larger than last evt_num {}"
                                 "".format(evt_num, last_evt_num))

            ch_array_sum[ch_array_sum > 3] = 3   #3 is max. threshold
            evtnum_bxing_clusters.append((last_evt_num, -1,
                                          cluster_algorithm(ch_array_max, channel_range=channels,
                                                            **setting)))
            evtnum_bxing_clusters.append((last_evt_num, -2,
                                          cluster_algorithm(ch_array_sum, channel_range=channels,
                                                            **setting)))
            ch_array_max[:] = 0
            ch_array_sum[:] = 0

            if options.Nrows and i >= options.Nrows:
                breaked = True
                break

        evtnum_bxing_clusters.append( (evt_num, BXing,
                                       cluster_algorithm(ch_array, channel_range=channels,
                                                         **setting)) )

        ch_array_max = np.maximum(ch_array_max, ch_array)
        ch_array_sum = ch_array_sum + ch_array


        last_evt_num = evt_num
        last_BXing = BXing


    #Store last pack of bunch crossings (if not stopped before)
    if not breaked:
        ch_array_sum[ch_array_sum > 3] = 3   #3 is max. threshold
        evtnum_bxing_clusters.append((last_evt_num, -1,
                                      cluster_algorithm(ch_array_max, channel_range=channels,
                                                        **setting)))
        evtnum_bxing_clusters.append((last_evt_num, -2,
                                      cluster_algorithm(ch_array_sum, channel_range=channels,
                                                        **setting)))

    #Build dataframe and store to file
    DF = pd.DataFrame(evtnum_bxing_clusters,
                      columns=["Evt_num", "BXing", "Cluster"])

    DF.to_hdf(options.Output, "PACIFIC", mode="w", complevel=1, complib="zlib")
