from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm, Cluster
import numpy as np
import root_numpy as rn
import pandas as pd
from argparse import ArgumentParser
import TBFunctions as TB
import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


#Channels to use for the clustering
channels = range(0, 64, 1)
SPIROC_channels = range(320, 384, 1)

#Channels to keep in the SPIROC data
#They will be overwritten with the threshold data
#The pixel-data will be accessible at Ch_Pixels_{number}
SPIROC_keep_channels = ["Ch_{}".format(ch) for ch in range(320, 384)]
SPIROC_pixel_channels = [ col.replace("Ch_", "Ch_Pixels_") for col in
                          SPIROC_keep_channels ]

#Data_columns for on-disk queries
PACIFIC_data_columns = [ "Layer", "BXing", "Integrator", "Finetiming", "TrackHit_Ch",
                         "Track_Chi2NDOF", "TelescopeClusters", "TrackHit_Y" ]
SPIROC_data_columns = [ "Layer", "TrackHit_Ch", "Track_Chi2NDOF", "TelescopeClusters",
                        "TrackHit_Y" ]


#Possible cluster algorithm settings to choose from
cluster_algo_settings = { "Default" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3},
                          #"No_requirements" : {"seed_th" : 2, "min_size" : 0, "single_ch_th" : 0},
                          #"Seed_1" : {"seed_th" : 1, "min_size" : 0, "single_ch_th" : 0},
                          #"Big" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3, "continue_th" : 1},
                        }



if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create clusters and merge into data")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-t", "--tree", dest="Tree", action="store", required=False,
                        choices=["PACIFIC", "SPIROC"], default="PACIFIC")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output HDF5-file")
    parser.add_argument( "-s", "--thsetting", dest="ThSetting", required=False,
                        help="Threshold setting (SPIROC only, e.g. 152535)")
    parser.add_argument( "-n", "--nevents", dest="Nevents", action="store", required=False,
                        type=int, help="Only process this amount of events.")


    #Parse arguments from command line
    options = parser.parse_args()

    if options.Tree == "SPIROC":
        #Check if -s is provided when -t is provided
        if not options.ThSetting:
            raise ValueError("Processing SPIROC data but not threshold setting was set!")

        elif len(options.ThSetting) != 6:
            raise ValueError("ThSetting must be a 6 digit number (e.g. 152535 for Ths 1.5 2.5 3.5)")

        else:
            Th1 = float(options.ThSetting[0]+"."+options.ThSetting[1])
            Th2 = float(options.ThSetting[2]+"."+options.ThSetting[3])
            Th3 = float(options.ThSetting[4]+"."+options.ThSetting[5])

            SPIROC_ThSetting = [ Th1, Th2, Th3 ]


    #==============================
    # PACIFIC ======================
    #==============================
    if options.Tree == "PACIFIC":
        print("PACIFIC data provided")
        #Open root file
        print("Reading file {}..., Tree {}".format(options.Input, options.Tree))
        if options.Nevents:
            nevents = options.Nevents
            print("Read only {} events/rows".format(nevents))
        else:
            nevents = None
        DF = pd.DataFrame( rn.root2array(options.Input, options.Tree, stop=nevents) )
        print("Loaded into pandas DataFrame ({} rows).".format(len(DF)))

        unique_BXings = np.unique(DF["BXing"])
        print("Found BXings: ", unique_BXings)

        data_columns = PACIFIC_data_columns
        cluster_channels = channels







    #==============================
    # SPIROC ======================
    #==============================
    if options.Tree == "SPIROC":
        print("SPIROC data provided")
        #Get columns from root-file
        all_columns = rn.list_branches(options.Input, options.Tree)
        columns = []
        for col in all_columns:  #Keep only specific Ch_ columns
            if "Ch_" not in col or col in SPIROC_keep_channels:
                columns.append(col)


        if options.Nevents:
            nevents = options.Nevents
            print("Read only {} events/rows".format(nevents))
        else:
            nevents = None

        print("Read columns {} from {}, Tree {}".format(columns, options.Input, options.Tree))
        DF = pd.DataFrame( rn.root2array(options.Input, options.Tree, branches=columns, stop=nevents) )
        print("Loaded into pandas DataFrame ({} rows).".format(len(DF)))

        #Transform SPIROC pixels into exceeded thresholds
        ThS = options.ThSetting
        print("Transforming channel pixels in thresholds ThS{} ({})"
              .format(ThS, SPIROC_ThSetting))
        DF[ SPIROC_pixel_channels ] = DF[ SPIROC_keep_channels ]
        DF[ SPIROC_keep_channels ] = TB.SPIROC2Ths( DF[ SPIROC_keep_channels ].values,
                                                    SPIROC_ThSetting )


        data_columns = SPIROC_data_columns
        cluster_channels = SPIROC_channels




    #=========
    #COMMON
    #=========
    #Save to HDF
    print("Saving Data to {}/Data...".format(options.Output))
    DF.to_hdf(options.Output, "Data", format="table", mode="w", complevel=1, complib="zlib",
              data_columns=data_columns)

    print("Performing clustering")
    chmatrix = DF[ ["Ch_{}".format(ch) for ch in cluster_channels] ].values
    index = DF.index.values
    del DF

    for name, settings in cluster_algo_settings.items():
        print(name, settings)
        DF_Clusters = pd.DataFrame(index=index)
        key = "Clusters_{}".format(name)
        DF_Clusters[key] = [ cluster_algorithm( charray, channel_range=cluster_channels,
                                               **settings) for charray in chmatrix ]

        #Save to HDF (every cluster configuration in a different key (performance reasons))
        print("Saving Clusters to {}".format(key))
        DF_Clusters.to_hdf(options.Output, key, format="fixed", mode="a",
                           complevel=1, complib="zlib")

        del DF_Clusters
