from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm
import numpy as np
import pandas as pd
import ROOT
from argparse import ArgumentParser
import TBFunctions as TB

#Get value from event
def getCurrentValue(event, branchName):
     return getattr(event,branchName)

#Channels where to perform clustering
#channels = np.arange(192, 256, 1)  #Chs 192...255 correspond to PACIFIC Chs 0...64
channels = np.arange(192, 241, 1)  #Exclude dead channel region on the right side

#Possible cluster algorithm settings to choose from
cluster_algo_settings = { "Default" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3},
                          "No_requirements" : {"seed_th" : 2, "min_size" : 0, "single_ch_th" : 0},
                          "Seed_1" : {"seed_th" : 1, "min_size" : 0, "single_ch_th" : 0},
                          "Big" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3, "continue_th" : 1}
                        }


if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create clusters and store them in a pickle file")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output Pickle-file")
    parser.add_argument( "-a", "--algorithm", dest="Algorithm", action="store", required=True,
                         help="Algorithm name to use")
    parser.add_argument( "-t", "--ths", dest="Ths", nargs=3, required=True, type=float,
                         help="Thresholds to use to convert into PACIFIC like data")
    parser.add_argument( "-n", "--nrows", dest="Nrows", action="store", required=False, type=int,
                         help="Lines to process at least (last event will be fully processed), "
                         "optional", default=None )

    #Parse arguments from command line
    options = parser.parse_args()


    setting = cluster_algo_settings[options.Algorithm]

    if options.Nrows:
        print("-n set. Will stop after processing {} entries.".format(options.Nrows))

    print("Using PACIFIC thresholds ", options.Ths)

    #Open tree
    inputfile = ROOT.TFile(options.Input, "READ")
    if not inputfile.IsOpen():
        raise SystemExit("Could not open inputfile!")

    inputtree = inputfile.Get("SPIROC")


    #Create list for evt_num, clusters tuple
    evtnum_clusters = []


    ch_pixels = np.zeros(len(channels), dtype=float)

    #Loop through TTree
    n_entries = inputtree.GetEntries()
    for i, event in enumerate(inputtree):
        if i % 10000 == 0:
            print("{} of {} entries processed ({:.2%})".format(i, n_entries, i/n_entries))
        evt_num = getCurrentValue(event,"Evt_num")
        for idx, ch_no in enumerate(channels):
            ch_pixels[idx] = getCurrentValue(event,"Ch_{}".format(ch_no))

        if options.Nrows and i >= options.Nrows:
            break

        ch_ths = TB.SPIROC2Ths(ch_pixels, Ths=options.Ths)
        evtnum_clusters.append( (evt_num, cluster_algorithm(ch_ths, channel_range=channels,
                                                            **setting)) )


    #Build dataframe and store to file
    DF = pd.DataFrame(evtnum_clusters,
                      columns=["Evt_num", "Cluster"])

    DF.to_hdf(options.Output, "SPIROC", mode="w", complevel=1, complib="zlib")
