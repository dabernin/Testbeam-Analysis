#!/usr/bin/env python
"""
Tool to plot the signalshape for a given data file
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
from TBFunctions import signalshape_new, div0, extract_Ths
import numpy as np
import pandas as pd
import root_numpy as rn
import argparse
import matplotlib.pyplot as plt
import danplotlib as dpl
import re
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors

plt.style.use("presentation")
gc_colors.update_colorcycle()


#TUNE THESE PARAMETERS
#=====================
nEvents = 5000000 #Number of events to read
FT_low = 13  #Lower finetiming window edge
FT_high = 19 #Higher ....


def PACIFIC2DF(*args, **kwargs):
    DF = pd.DataFrame( rn.root2array(*args, **kwargs) )
    all_BXs = np.unique(DF["BXing"])
    if len(DF) % len(all_BXs) != 0:
        print("WARNING: Not all BXs loaded completely!")

    return DF


def plot_signalshape(pdf, DF, mode="eq", BXs=range(8), cut_BX_val=None,
                     title="", FT_shift=0, ThNames=None):


    fig, axes = signalshape_new(
        DF["BXing"], (DF["Finetiming"]+FT_shift)%32,
        DF[["Ch_{}".format(ch) for ch in range(64)]].values,
        mode=mode, BXs=BXs, cut_BX_val=cut_BX_val, figsize=(20,10), ThNames=ThNames)


    fig.suptitle(title+", FT shift: {}".format(FT_shift), fontsize=20)
    plt.legend(fontsize=22, frameon=True, framealpha=1, edgecolor="none")
    pdf.savefig(fig)

    """
    #Also plot selection
    for axis in axes:
        axis.axvspan(FT_low+FT_shift, FT_high+FT_shift, color="gcgreen",
                     label="FT selection", alpha=0.2)
    plt.legend(fontsize=22, frameon=True, framealpha=1, edgecolor="none")
    pdf.savefig(fig)
    """
    plt.close()


    #Plot ratio of BX2/BX1
    for lineno in [0,1,2,3]:
        lineno_BX1 = cut_BX_val[1]-1 if cut_BX_val else lineno
        BX1 = axes[1].lines[lineno_BX1]
        BX2 = axes[2].lines[lineno]
        #Labels
        if ThNames:
            ThNames_label = ThNames + ["\infty"]
            #BX2 (depends on plotting mode)
            if lineno == 3: #weighted mean
                labelBX2 = "Weighted mean (BX2)"
            elif mode == "geq" or mode == "eq|geq":
                labelBX2 = "[${},\infty$) pes (BX2)".format(ThNames_label[lineno])
            elif mode == "eq":
                labelBX2 = "[${},{}$) pes (BX2)".format(ThNames_label[lineno],
                                                        ThNames_label[lineno+1])
            #BX1
            if lineno == 3 and not cut_BX_val:
                labelBX1 = "Weighted mean (BX1)"
            elif mode == "eq" or mode == "eq|geq":
                labelBX1 = "[${},{}$) pes (BX1)".format(ThNames_label[lineno_BX1],
                                                        ThNames_label[lineno_BX1+1])
            elif mode == "geq":
                labelBX1 = "[${},\infty$) pes (BX1)".format(ThNames_label[lineno_BX1])

            label = labelBX2+" /\n"+labelBX1
        else:
            if lineno == 3:
                label = "Weighted mean (BX2) / Th{} (BX1)".format(lineno_BX1+1)
            else:
                label = "Th{} (BX2) / Th{} (BX1)".format(lineno+1, lineno_BX1+1)



        if not np.all([BX1.get_xdata(), BX2.get_xdata()]):
            raise ValueError("Finetimings do not match btw BX1 and BX2!")
        FTs = BX1.get_xdata()
        ratio = div0(BX2.get_ydata(), BX1.get_ydata())
        dpl.errorbar(FTs, ratio, xerr=0.5, yerr=0, fmt=".", ms=0, label=label,
                     color="k" if lineno==3 else None)

    dpl.xlabel("Finetiming")
    dpl.ylabel("BX2/BX1")
    plt.xlim(0, 32)
    plt.minorticks_on()
    plt.title((title+", FT shift: {}"+"\nRatio BX2/BX1")
              .format(FT_shift), fontsize=20)
    plt.legend(fontsize=18)
    plt.gca().invert_xaxis()
    pdf.savefig()
    plt.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot signalshape")
    parser.add_argument("file", help="File to process (PACIFIC)")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")
    parser.add_argument("--FTshift", "-f", help="Finetiming shift",
                        type=int, default=0)


    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.file))[0]
                      +"_Signalshape.pdf" )

    DF = PACIFIC2DF(args.file, "PACIFIC", stop=nEvents)
    print("Read {} entries from {}.".format(len(DF), args.file))

    ThNames = extract_Ths(args.file)

    with PdfPages(args.save) as pdf:
        #Raw signalshape
        #EQUAL
        plot_signalshape(pdf, DF, mode="eq", title="No cuts, equal",
                         FT_shift=args.FTshift, ThNames=ThNames)

        #GREATER EQUAL
        plot_signalshape(pdf, DF, mode="geq", title="No cut, greater equal",
                         FT_shift=args.FTshift, ThNames=ThNames)


        for value in [1,2,3]:
            #EQUAL
            plot_signalshape(pdf, DF, mode="eq", cut_BX_val=[1,value],
                             title="Signal = Th {} in BX 1, equal"
                                   "".format(value), FT_shift=args.FTshift, ThNames=ThNames)

            #GREATER EQUAL
            plot_signalshape(pdf, DF, mode="geq", cut_BX_val=[1,value],
                             title="Signal $\geq$ Th {} in BX 1, greater equal"
                                   "".format(value), FT_shift=args.FTshift, ThNames=ThNames)

            #COMBINATION OF BOTH
            plot_signalshape(pdf, DF, mode="eq|geq", cut_BX_val=[1,value],
                             title="Signal = Th {} in BX 1, greater equal"
                                   "".format(value), FT_shift=args.FTshift, ThNames=ThNames)

    print("DONE. Saved plots to {}.".format(args.save))
