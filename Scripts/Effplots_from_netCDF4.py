#!/usr/bin/env python
"""
Tool to plot the efficiency finetiming scan
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
#import ROOT
#ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
import TBFunctions as TB
import numpy as np
import xarray as xr
import argparse
import matplotlib.pyplot as plt
import danplotlib as dpl
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors
import itertools as iter

plt.style.use("presentation")
gc_colors.colorcycle.append("gclime")
gc_colors.update_colorcycle()



#General settings
algos = ["Default"]
FTshift = -2
Chsel = range(3,20) + range(29, 48)  #Channels to calc efficiency for
grid = True   #Show grid?

#Finetimingscan settings
BXs = [0,1,2,3,4]  #BXs to show
FT_sel_extended = range(7, 22)   #Extended view for finetiming scan #2

#Good signal selection
BX_sel = 1   #Calc efficiency for this BX
FT_sel = range(12,17)  #Calc efficiency for these FTs  (without shift)

#Efficiency vs. channels
BXs_chs = [1,2,3,4]    #Show these BXs
FT_sel = FT_sel  #Select these FTs


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot efficiency finetiming scan.")
    parser.add_argument("Input", help="netCDF4-file to process")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")
    parser.add_argument("--compare", "-c", help="SPIROC netCDF4-file to "
                        "compare efficiency plot vs. channels with", default="")


    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.Input))[0]
                     +"_Effplots.pdf" )

    #SPIROC data provided?
    if "SPIROC" in args.Input:
        print("SPIROC data detected!")
        inputtype = "SPIROC"
        FT_sel = [0]
        Chsel = [ 320 + ch for ch in Chsel ]
        BX_sel = 0
        BXs_chs = [0]    #Show these BXs
    else:
        print("Assuming PACIFIC data!")
        inputtype = "PACIFIC"


    #Create PDF
    with PdfPages(args.save) as pdf:
        #Loop through cluster algorithms
        for algo in algos:
            print("Cluster algorithms: {}".format(algo))
            #Open dataset
            with xr.open_dataset(args.Input, algo) as ds:
                print("Loaded netCDF4-file/group {}/{}".format(args.Input,
                                                               algo))
                # PACIFIC ONLY
                if inputtype == "PACIFIC":
                    #======Plot finetiming-scan======
                    print("Performing finetiming scan (accumulated)...")
                    #Select specific channels
                    ds_sel = ds.sel(Ch=Chsel)
                    #Sum over channels and runs
                    ds_sum = ds_sel.sum(["Ch", "Run_num"])
                    #Flatten the dataset
                    ds_flat = ds_sum.stack(BX_FT=("BX", "FT"))
                    #Calc eff
                    eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                        ds_flat["Trackhits_found"], ds_flat["Trackhits_total"])

                    #Gen plot
                    fig, axes = TB.finetimingplot(
                        ds_flat["BX"], (ds_flat["FT"]+FTshift)%32, eff,
                        BXs=BXs, errbarsts={"yerr" : [eff_errlow, eff_errup],
                                            "label" : "PACIFIC"})

                    fig.suptitle("Finetiming scan,, cluster algorithm {}\n"
                                 "ALL selected channels,"
                                 " FT shift: {}".format(algo, FTshift))
                    axes[0].set_ylabel("Efficiency")
                    axes[-1].set_xlabel("Finetiming")

                    #Indicate finetiming selection
                    for axis in axes:
                        for idx, FT in enumerate(FT_sel):
                            FT = (FT+FTshift)%32
                            axis.axvspan(FT-0.5, FT+0.5, color="gcorange",
                                         alpha=0.15, linewidth=0,
                                         label="FT sel" if idx==0 else "")

                        #Activate grid?
                        if grid:
                            axis.grid(ls="--")


                    #SPIROC compare data provided?
                    if args.compare:
                        with xr.open_dataset(args.compare, algo) as ds_SPIROC:
                            #Select channels
                            Chsel_SPIROC = [ 320 + ch for ch in Chsel ]
                            ds_SPIROC_sel = ds_SPIROC.sel(Ch=Chsel_SPIROC)
                            #Sum up everything
                            ds_SPIROC_sum = ds_SPIROC_sel.sum(
                                ["Run_num", "FT", "BX", "Ch"])

                            eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                                ds_SPIROC_sum["Trackhits_found"].item(),
                                ds_SPIROC_sum["Trackhits_total"].item())

                            #Plot in each axis
                            for axis in axes:
                                axis.axhline(eff, color="gcred", label="SPIROC")
                                axis.axhspan(eff-eff_errlow, eff+eff_errup,
                                             color="gcred", label="",
                                             alpha=0.3)


                    axes[-1].legend(fontsize=18)


                    pdf.savefig(fig)
                    plt.close()



                    #======Plot finetiming-scan for individual channels======
                    print("Performing finetiming scan (individual)...")
                    #Select specific channels
                    ds_sel = ds.sel(Ch=Chsel)
                    #Sum over run numbers
                    ds_sum = ds_sel.sum("Run_num")
                    #Generate the (empty) plot
                    fig, axes = dpl.subplots(1, len(BXs), sharey=True,
                                             figsize=(16,10))
                    #Loop over channels
                    lstyles = iter.cycle(["-", "--", "-.", ":"])
                    for ch, group in ds_sum.groupby("Ch"):
                        #Shift the FTs and sort by new FTs
                        group["FT"] = (group["FT"]+FTshift)%32
                        group = group.sortby("FT")
                        #Flatten the dataset
                        ds_flat = group.stack(BX_FT=("BX", "FT"))
                        #Calc eff
                        eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                            ds_flat["Trackhits_found"], ds_flat["Trackhits_total"])

                        #Plot for this channel
                        fig, axes = TB.finetimingplot(
                            ds_flat["BX"], ds_flat["FT"], eff,
                            BXs=BXs, fig=fig, axes=axes,
                            errbarsts={"xerr" : 0, "yerr" : 0,
                                       "ls" : lstyles.next(), "lw" : 1,
                                       "label" : "Ch{}".format(ch)})


                    fig.suptitle("Finetiming scan, cluster algorithm {}\n"
                                 "Individual channels, "
                                 "FT shift: {}".format(algo, FTshift))
                    axes[0].set_ylabel("Efficiency")
                    axes[-1].set_xlabel("Finetiming")

                    #Indicate finetiming selection
                    for axis in axes:
                        for idx, FT in enumerate(FT_sel):
                            FT = (FT+FTshift)%32
                            axis.axvspan(FT-0.5, FT+0.5, color="gcorange",
                                         alpha=0.15, linewidth=0,
                                         label="FT sel" if idx==0 else "")

                        #Activate grid?
                        if grid:
                            axis.grid(ls="--")

                    axes[-1].legend(fontsize=10)


                    pdf.savefig(fig)
                    plt.close()





                    #=====Plot finetiming scan below each other around FT sel=====
                    print("Performing finetiming scan #2 (accumulated)...")
                    #Select specific channels
                    ds_sel = ds.sel(Ch=Chsel)
                    #Sum over channels and runs
                    ds_sum = ds_sel.sum(["Ch", "Run_num"])
                    #Gen figure
                    fig, axes = dpl.subplots(len(BXs), sharex=True,
                                             figsize=(10,20))
                    if not hasattr(axes, "__iter__"):
                        axes = [axes]
                    #Loop through BXs
                    for BX, axis in zip(BXs, axes):
                        ds_BX = ds_sum.sel(BX=BX)
                        #Calc eff
                        eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                            ds_BX["Trackhits_found"], ds_BX["Trackhits_total"])
                        #Plot
                        axis.errorbar((ds_BX["FT"]+FTshift)%32, eff, xerr=0.5,
                                      yerr=[eff_errlow, eff_errup],
                                      label="BX {}".format(BX))
                        #Limit view around selected FTs
                        axis.set_xlim((FT_sel_extended[0]+FTshift)%32,
                                      (FT_sel_extended[-1]+FTshift)%32)

                        #Autoscale (do not remove outliers though)
                        axis.autoscale_nooutliers(thresh=0)

                        #Indicate FT selection
                        for idx, FT in enumerate(FT_sel):
                            FT = (FT+FTshift)%32
                            label = "FT sel" if idx == 0 else ""
                            axis.axvspan(FT-0.5, FT+0.5, color="gcorange",
                                         alpha=0.15, linewidth=0,
                                         label=label)


                        #SPIROC compare data provided?
                        #Only for "right" BX
                        if BX == BX_sel:
                            if args.compare:
                                with xr.open_dataset(args.compare, algo) as ds_SPIROC:
                                    #Select channels
                                    Chsel_SPIROC = [ 320 + ch for ch in Chsel ]
                                    ds_SPIROC_sel = ds_SPIROC.sel(Ch=Chsel_SPIROC)
                                    #Sum up everything
                                    ds_SPIROC_sum = ds_SPIROC_sel.sum(
                                        ["Run_num", "FT", "BX", "Ch"])

                                    eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                                        ds_SPIROC_sum["Trackhits_found"].item(),
                                        ds_SPIROC_sum["Trackhits_total"].item())

                                    #Plot
                                    axis.axhline(eff, color="gcred", label="SPIROC")
                                    axis.axhspan(eff-eff_errlow, eff+eff_errup,
                                                 color="gcred",label="",
                                                 alpha=0.3)

                                    ylim = axis.get_ylim()
                                    axis.set_ylim(ylim[0], 1+0.05*(ylim[1]-ylim[0]))




                        axis.legend(fontsize=18, frameon=True, framealpha=1,
                                    edgecolor="none")

                        #Activate grid?
                        if grid:
                            axis.grid(ls="--")

                    axes[0].set_title("Finetiming scan #2, cluster algorithm {}, "
                                      "FT shift: {}".format(algo, FTshift))
                    axes[-1].set_xlabel("Finetiming")
                    axes[0].set_ylabel("Efficiency")

                    fig.subplots_adjust(hspace=0)
                    pdf.savefig(fig)
                    plt.close()





                #=====Plot efficiency over channels==========
                print("Plotting efficiency vs. channels...")
                #Select specific FTs
                ds_sel = ds.sel(FT=FT_sel)
                #Aggregate over runs and remaining FTs
                ds_sum = ds_sel.sum(["Run_num", "FT"])
                #Loop through BXs
                markers = iter.cycle(["x", "o", "^", "*", "+", "."])
                fig, axis = dpl.subplots(1, figsize=(12,8))
                for BX, group in ds_sum.groupby("BX"):
                    if BX in BXs_chs:  #Only plot when specified
                        #Calc efficiency for all channels for this BX
                        eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                            group["Trackhits_found"],
                            group["Trackhits_total"])

                        #Total efficiency for selected channels
                        group_tot = group.sel(Ch=Chsel).sum("Ch")
                        eff_tot = TB.calc_TEfficiency(
                            group_tot["Trackhits_found"].item(),
                            group_tot["Trackhits_total"].item())

                        chs = group["Ch"]
                        if len(BXs_chs) == 1:
                            label="PACIFIC"
                        else:
                            label="BX {}".format(BX)

                        label+=(r", $\bar{{\epsilon}} = ({:.2f}\pm{:.2f})\,\%$"
                                "".format(eff_tot[0]*100,
                                          np.mean(eff_tot[1:])*100))

                        axis.errorbar(chs, eff, xerr=0.5, fmt=markers.next(),
                                     yerr=[eff_errlow, eff_errup], ms=5,
                                     label=label)

                #Compare with SPIROC?
                if args.compare:
                    with xr.open_dataset(args.compare, algo) as ds_SPIROC:
                        ds_SPIROC_sum = ds_SPIROC.sum(["Run_num", "FT", "BX"])
                        eff, eff_errlow, eff_errup = TB.calc_TEfficiency(
                            ds_SPIROC_sum["Trackhits_found"],
                            ds_SPIROC_sum["Trackhits_total"])

                        #Total efficiency for selected channels
                        Chsel_SPIROC = [ ch+320 for ch in Chsel ]
                        ds_tot = ds_SPIROC_sum.sel(Ch=Chsel_SPIROC).sum("Ch")
                        eff_tot = TB.calc_TEfficiency(
                            ds_tot["Trackhits_found"].item(),
                            ds_tot["Trackhits_total"].item())

                        label="SPIROC"
                        label+=(r"$, \bar{{\epsilon}} = ({:.2f}\pm{:.2f})\,\%$"
                                "".format(eff_tot[0]*100,
                                          np.mean(eff_tot[1:])*100))

                        chs = ds_SPIROC_sum["Ch"]-320
                        axis.errorbar(chs, eff, xerr=0.5, fmt=markers.next(),
                                     yerr=[eff_errlow, eff_errup], ms=5,
                                     label=label)

                axis.autoscale_nooutliers()

                #Activate grid?
                if grid:
                    axis.grid(ls="--")

                #Indicate channel selection
                for idx, ch in enumerate(Chsel):
                    plt.axvspan(ch-0.5, ch+0.5, color="gcorange",
                                alpha=0.15, linewidth=0,
                                label="Ch sel" if idx==0 else "")

                dpl.xlabel("Channel")
                dpl.ylabel("Efficiency")
                plt.title("Efficiency vs. channels, cluster algorithm {}\n"
                          "FTs {}".format(algo, FT_sel),fontsize=20)
                plt.legend(fontsize=16)

                pdf.savefig(fig)
                plt.close()








                #======Plot efficiency over run numbers=======
                print("Performing run scan...")
                #Select specific channels, BX and FTs
                ds_sel = ds.sel(Ch=Chsel, BX=BX_sel, FT=FT_sel)
                #Loop through run numbers
                run_nums = []
                effs = []
                for run_num, group in ds_sel.groupby("Run_num"):
                    #Sum over remaining dimensions
                    group_sum = group.sum()
                    eff = TB.calc_TEfficiency(
                        group_sum["Trackhits_found"].item(),
                        group_sum["Trackhits_total"].item())
                    run_nums.append(run_num)
                    effs.append(eff)

                #Plot
                effs = np.asarray(effs)
                run_nums = np.asarray(run_nums)
                if len(run_nums) > 1:
                    xerr = np.min(run_nums[1:]-run_nums[:-1])/4
                else:
                    xerr = 1
                dpl.errorbar(run_nums, effs[:,0], xerr=xerr,
                             yerr=[ effs[:,1], effs[:,2] ], ms=5)
                plt.xlabel("Run")
                dpl.ylabel("Efficiency")
                plt.title("Run number scan, cluster algorithm: {}\n"
                          "BX {}, FTs {}".format(algo, BX_sel, FT_sel))

                #Activate grid?
                if grid:
                    plt.grid(ls="--")

                pdf.savefig()
                plt.close()

    print("DONE. Saved plots to {}.".format(args.save))
