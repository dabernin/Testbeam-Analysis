#!/usr/bin/env python
"""
Tool to plot the efficiency (and related stuff) for given data files
(typically PACIFIC and SPIROC for comparison reasons)
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
from rootpy.io import root_open
from rootpy.plotting import Hist
import root_numpy as rn
import TBFunctions as TB
import numpy as np
import pandas as pd
from scipy import stats
import argparse
import matplotlib.pyplot as plt
import danplotlib as dpl
from itertools import izip, cycle, chain
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors

plt.style.use("presentation")
gc_colors.update_colorcycle()
color_cycle=["gcblue", "gcred", "gcgreen", "gcorange","gccyan", "gcmagenta",
             "gcbrown", "gcdarkgrey"]

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

#Misc settings
algos = ["Default"]
channels = range(320, 384)
ch_offset = channels[0]
ch_columns = [ "Ch_Pixels_{}".format(ch) for ch in channels ]
columns = ch_columns + ["TrackHit_Ch"]
columns += ["Clusters_{}".format(algo) for algo in algos]
nEvents = None  #Only read that amount of events, set to None if you want all
pes = range(5) + range(5, 31, 5)
plot_hists = ["In Cluster", "Cluster Center", "At Track"]

#Plotting settings
binning = np.linspace(-0.5, 20.5, 500)  #In direction of light yield (1D plot)
ROOT_binning = np.linspace(-1, 50, 1200)
binning_2D = [ np.arange(-0.5, 64, 1), np.linspace(-0.5, 20.5, 100) ]

logy = False
norm = True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot SPIROC lightyield")
    parser.add_argument("SPIROCData", help="File to process")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")
    parser.add_argument("--results", "-r", help="Where to save the results "
                        "as .csv", default="")
    parser.add_argument("--root", help="Where to save the histograms "
                        "as .root", default="")


    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.SPIROCData))[0]
                     +"_Lightyield.pdf" )


    DF = TB.HDF2DF(args.SPIROCData, clusteralgos=algos, columns=columns,
                   PixelData=True, stop=nEvents)
    eprint("{}: Read {} events".format(args.SPIROCData, len(DF)))


    #Create result DataFrame
    DF_results = pd.DataFrame(index=range(len(channels)))
    DF_results.index.name = "Ch"

    #List to contain root histograms for later writing
    ROOTHists = []

    with PdfPages(args.save) as pdf:
        for algo in algos:
            print("Cluster algorithm: {}".format(algo))
            print(30*"-")

            #Preparation
            #Save pixel information
            hist_dict = { "In Cluster" : [ [] for i in channels ],
                         "Cluster Center"  : [ [] for i in channels ],
                         "At Track" : [ [] for i in channels ],
                         "Single Ch Cluster" : [ [] for i in channels ],
                         "Single Ch Cluster near track" : [ [] for i in channels ],
                         "Cluster lightyield" : [ [] for i in channels ],
                         "2 Chs Cluster lightyield" : [ [] for i in channels ],
                         "3 Chs Cluster lightyield" : [ [] for i in channels ],
            }

            #Special histogram
            cluster_width = []


            #Calculation
            pixel_data = DF[ch_columns].values
            clusters = DF["Clusters_{}".format(algo)]
            trackhits = np.rint(DF["TrackHit_Ch"]).astype(int)
            for pixels, clusters, trackhit in izip(pixel_data, clusters, trackhits):
                for cluster in clusters:
                    #Cluster width
                    cluster_width.append(cluster.Size())

                    #Cluster Center
                    mid = np.rint(cluster.Position()+1e-10).astype(int)
                    hist_dict["Cluster Center"][mid-ch_offset].append(
                        pixels[mid-ch_offset])

                    #Single channel cluster
                    if cluster.Size() == 1:
                        ch = cluster.Channels[0]-ch_offset
                        hist_dict["Single Ch Cluster"][ch].append(pixels[ch])
                        #Single channel cluster near track
                        if abs(cluster.Position()-trackhit) < 4:
                            hist_dict["Single Ch Cluster near track"][ch].append(
                                pixels[ch])

                    #All channels in cluster
                    cluster_lightyield = 0
                    for ch in cluster.Channels:
                        ch_corr = ch-ch_offset
                        pixel = pixels[ch-ch_offset]
                        hist_dict["In Cluster"][ch_corr].append(pixel)
                        cluster_lightyield += pixel

                    #Cluster lightyield (all clusters)
                    hist_dict["Cluster lightyield"][mid-ch_offset].append(
                        cluster_lightyield)

                    #Cluster lightyield (2 Chs clusters)
                    if cluster.Size() == 2:
                        hist_dict["2 Chs Cluster lightyield"][mid-ch_offset].append(
                            cluster_lightyield)

                    #Cluster lightyield (3 Chs clusters)
                    if cluster.Size() == 3:
                        hist_dict["3 Chs Cluster lightyield"][mid-ch_offset].append(
                            cluster_lightyield)

                #Track positions
                if trackhit >= channels[0] and trackhit <= channels[-1]:
                    hist_dict["At Track"][trackhit-ch_offset].append(
                        pixels[trackhit-ch_offset])



            #Print and plot the 1D projection for all conditions in one plot
            colors = cycle(color_cycle)
            label_plotted = False
            for descr, hist in sorted(hist_dict.iteritems()):
                #Flatten the data
                hist_arr = np.asarray(list(chain.from_iterable(hist)))

                #Create root histogram
                ROOTHist = Hist(len(ROOT_binning), ROOT_binning[0],
                                ROOT_binning[-1], name=descr, title=descr)
                rn.fill_hist(ROOTHist, hist_arr)
                ROOTHists.append(ROOTHist)

                #Only plot when contained in plot_hists
                if descr in plot_hists:
                    color = colors.next()
                    mean = np.mean(hist_arr)
                    print("=={}==".format(descr))
                    print("Mean lightyield: {:.1f} pes".format(mean))
                    for pe in pes:
                        eff = TB.calc_TEfficiency(len(hist_arr[hist_arr>pe]),
                                                  len(hist_arr))
                        print(">{:3} pes: ({:5.2f} +/- {:5.2f})%"
                              "".format(pe, eff[0]*100, np.mean(eff[1:])*100))

                    dpl.hist(hist_arr, bins=binning, log=logy, label=descr,
                             normed=norm, color=color)
                    if label_plotted:
                        plt.axvline(mean, ls="--", color=color)
                    else:
                        plt.axvline(mean, ls="--", color=color, label="Mean")
                        label_plotted=True

                    print("\n")

            plt.title("Cluster algorithm: {}".format(algo))
            dpl.xlabel("Photo electrons")
            dpl.ylabel("Normalised entries" if norm else "Entries")
            plt.legend(fontsize=20, frameon=True, edgecolor="none",
                       framealpha=1)
            plt.xlim(binning[0], binning[-1])
            plt.show()
            pdf.savefig()
            plt.close()


            #Plot 2D histogram of pes vs. channel
            for descr, hist in sorted(hist_dict.iteritems()):
                ch_pe_arr = np.asarray([ [ch, pixel]
                                          for ch, pixel_list in enumerate(hist)
                                          for pixel in pixel_list ])
                channels_arr = ch_pe_arr[:,0]
                pes_arr = ch_pe_arr[:,1]
                plt.hist2d(channels_arr, pes_arr, bins=binning_2D)

                #Calc and plot means
                ch_mean_arr = np.asarray([[ch,np.mean(pixels) if pixels else 0,
                                           stats.sem(pixels) if pixels else 0]
                                           for ch, pixels in enumerate(hist)])
                dpl.errorbar(ch_mean_arr[:,0], ch_mean_arr[:,1], xerr=0.5,
                             yerr=ch_mean_arr[:,2], color="gcred", ls="none")

                #Save mean lightyields to dataframe
                colname = "{}_{}".format(algo, descr).replace(" ", "")
                DF_results[colname+"_Mean"] = ch_mean_arr[:,1]
                DF_results[colname+"_Meanerr"] = ch_mean_arr[:,2]

                plt.minorticks_on()
                plt.colorbar()
                dpl.xlabel("Channel")
                dpl.ylabel("Photo electrons")
                plt.title("Cluster algorithm: {}, {}".format(algo, descr))
                plt.show()
                pdf.savefig()
                plt.close()

    eprint("DONE. Saved plots to {}.".format(args.save))
    if args.results:
        DF_results.to_csv(args.results, sep="\t", float_format="%.3f")
        eprint("Saved lightyields to {}.".format(args.results))

    if args.root:
        with root_open(args.root, "recreate") as rootfile:
            for ROOTHist in ROOTHists:
                ROOTHist.Write()

            #Also save cluster widhts
            ROOTHist = Hist(21, -0.5, 20.5, name="Cluster width",
                            title="Cluster width")
            rn.fill_hist(ROOTHist, cluster_width)
            ROOTHist.Write()

        eprint("Saved histograms to {}".format(args.root))
