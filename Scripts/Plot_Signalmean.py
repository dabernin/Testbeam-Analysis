#!/usr/bin/env python
"""
Tool to plot the signalshape for a given data file
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
import TBFunctions as TB
import numpy as np
import pandas as pd
import root_numpy as rn
import argparse
import matplotlib.pyplot as plt
import danplotlib as dpl
import re
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors

plt.style.use("presentation")
gc_colors.update_colorcycle()


#TUNE THESE PARAMETERS
#=====================
nEvents = 5000000 #Number of events to read
FT_low = 12  #Lower finetiming window edge
FT_high = 16 #Higher ....

show_BXs = range(1,4,1)
sel = " || ".join(["BXing == {}".format(BX) for BX in show_BXs])

plot_channels = range(63)
#calc_channels = { "Layer_0" : range(5, 45), #51 Dead
#                  "Layer_1" : range(5, 20) + range(30, 55) } #22,26 short

#Common calc_channels
chrange = range(3,20) + range(29, 48)
calc_channels = { "Layer_0" : chrange, "Layer_1" : chrange }

def PACIFIC2DF(*args, **kwargs):
    DF = pd.DataFrame( rn.root2array(*args, **kwargs) )
    all_BXs = np.unique(DF["BXing"])
    if len(DF) % len(all_BXs) != 0:
        print("WARNING: Not all BXs loaded completely!")

    return DF


def extract_layer(file):
     m = re.search("Layer_\d", file)
     if m:
         return m.group(0)
     else:
         raise ValueError("Could not extract Layer from file {}".format(file))



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot mean signal (threshold)"
                                     " - excluding zeros!")
    parser.add_argument("file", help="File to process (PACIFIC)")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")
    parser.add_argument("--FTshift", "-f", help="Finetiming shift",
                        type=int, default=0)


    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.file))[0]
                      +"_Signalmean.pdf" )

    DF = PACIFIC2DF(args.file, "PACIFIC", stop=nEvents, selection=sel)
    print("Read {} entries from {}.".format(len(DF), args.file))
    print("(Selection: {})".format(sel))
    Layer = extract_layer(args.file)
    calc_channels = calc_channels[Layer]


    with PdfPages(args.save) as pdf:
        #Plot vs. finetiming
        print("Plot signalmean vs finetiming")
        print("{} detected, using channels {} for calculating the mean "
              "signalmean across channels".format(Layer, calc_channels))

        fig, axes = TB.signalmean(
            DF["BXing"], (DF["Finetiming"]+args.FTshift)%32,
            DF[["Ch_{}".format(ch) for ch in calc_channels]].values,
            BXs=show_BXs)

        #Indicate FT selection
        for axis in axes:
            axis.axvspan((FT_low+args.FTshift)%32-0.5,
                         (FT_high+args.FTshift)%32+0.5,
                         color="gcgrey", alpha=0.13,
                         linewidth=0, label="FT selection")
        axes[-1].legend(fontsize=20)
        fig.suptitle("Signalmean, FT shift: {}".format(args.FTshift),
                     fontsize=20)
        pdf.savefig(fig)
        plt.close()


        #Plot vs. channels
        print("Plot signalmean vs channels")
        print("Finetiming selection: [{}, {}]".format(FT_low, FT_high))
        DF_FTsel = DF[ (DF["Finetiming"] >= FT_low) &
                       (DF["Finetiming"] <= FT_high) ]
        print("{:.1%} events/rows survive selection"
              "".format(len(DF_FTsel)/len(DF)))

        for BX in show_BXs:
            DF_BX = DF_FTsel[ DF_FTsel["BXing"] == BX ]
            ch_mean, ch_mean_err = TB.masked_mean_error(
                DF_BX[["Ch_{}".format(ch) for ch in plot_channels]].values,
                axis=0)

            dpl.errorbar(plot_channels, ch_mean, xerr=0.5, yerr=ch_mean_err,
                         fmt=".", ms=0, label="BX {}".format(BX))

        plt.xlim(plot_channels[0]-0.5, plot_channels[-1]+0.5)

        #Indicate channel selection
        legend_shown = False
        for calcch in calc_channels:
            if legend_shown:
                plt.axvspan(calcch-0.5, calcch+0.5, color="gcgrey", alpha=0.13,
                            linewidth=0)
            else:
                plt.axvspan(calcch-0.5, calcch+0.5, color="gcgrey", alpha=0.13,
                            linewidth=0, label="Channel selection")
                legend_shown=True

        dpl.xlabel("Channel")
        dpl.ylabel("Mean Threshold")
        plt.legend(fontsize=20)
        pdf.savefig()
        plt.close()


    print("DONE. Saved plots to {}.".format(args.save))
