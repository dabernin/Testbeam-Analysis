#!/usr/bin/env python
"""
Tool to calculate found and total trackhits.
Can be used later to calculate the efficiency and purity.
"""
from __future__ import print_function, division

import sys
sys.path.append("../") #Make TBFunctions accessible
#import ROOT
#ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
import TBFunctions as TB
import numpy as np
import xarray as xr
import argparse

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


#Misc settings
algos = ["Default"]
resolution = 4

#Ranges to process (PACIFIC)
BXs = range(-2, 8)
FTs = range(32)
Chs = range(64)
#Run nums deducted from file

#Ranges to process (SPIROC)
BXs_SPIROC = [ 0 ]  #Dummy
FTs_SPIROC = [ 0 ]  #Dummy
Chs_SPIROC = range(0+320, 64+320)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Calculate number of trackhits")
    parser.add_argument("Input", help="HDF File to process")
    parser.add_argument("Output", help="Output netCDF4 file")

    args = parser.parse_args()

    #Determine type of measurement
    type = "SPIROC" if "SPIROC" in args.Input else "PACIFIC"
    print("{} data as input detected".format(type))

    #Get run numbers
    DF = TB.HDF2DF(args.Input, clusteralgos=[],
                   BXs = [ BXs[0] ] if type=="PACIFIC" else None)
    run_nums = np.unique( DF["Run_num"] )
    eprint("Found run numbers {}".format(run_nums))

   #Type dependant preparations of dimensions
    if type == "SPIROC":
        BXs = BXs_SPIROC
        FTs = FTs_SPIROC
        Chs = Chs_SPIROC

    #Dimensions
    dims = ["BX", "Run_num", "FT", "Ch" ]
    n_dims = [ len(BXs), len(run_nums), len(FTs), len(Chs) ]
    coords = [ BXs, run_nums, FTs, Chs ]

    #Loop through cluster algorithms
    for i, algo in enumerate(algos):
        mode = "w" if i == 0 else "a"
        eprint("Cluster algorithm {}".format(algo))

        #Generate xarray.Dataset
        ds = xr.Dataset(
            { "Trackhits_found" : ( dims, -1+np.zeros(n_dims, np.int64) ),
              "Trackhits_total" : ( dims, -1+np.zeros(n_dims, np.int64) ),
              "Clusters_found" : ( dims, -1+np.zeros(n_dims, np.int64) ),
              "Clusters_total" : ( dims, -1+np.zeros(n_dims, np.int64) ),
              "nEvents" : ( ("Run_num", "FT") ,
                           -1+np.zeros([len(run_nums), len(FTs)], np.int64))
             },
            coords=dict(zip(dims, coords)),
            attrs = { "Clusteralgorithm" : "Default",
                      "Resolution" : resolution } )


        #Perform calculation
        for n_BX, BX in enumerate(BXs):
            eprint("BX {}".format(BX))
            #Only read certain BX for PACIFIC
            DF_BX = TB.HDF2DF(args.Input, [algo],
                              BXs = [BX] if type == "PACIFIC" else None)
            for run_num in run_nums:
                DF_run = DF_BX[ DF_BX["Run_num"] == run_num ]
                eprint("Run {}".format(run_num))
                for FT in FTs:
                    if type == "PACIFIC":  #Select FT for PACIFIC
                        DF_FT = DF_run[ DF_run["Finetiming"] == FT ]
                    else:
                        DF_FT = DF_run
                    n_events = len(DF_FT)
                    eprint("FT {} ({} events)".format(FT, n_events))
                    if n_BX == 0:   #Save number of events for this run and FT
                        ds["nEvents"].loc[run_num, FT] = n_events
                    elif ds["nEvents"].loc[run_num, FT] != n_events:
                        raise ValueError("Number of events differs from "
                                         "reference BX {}!".format(BXs[0]))
                    trackhits = DF_FT["TrackHit_Ch"]
                    clusters = DF_FT["Clusters_{}".format(algo)]
                    t_fnd, t_tot, c_fnd, c_tot, _ = TB.cluster_efficiencies(
                        trackhits, clusters, resolution, Chs, True)

                    #Write to dataset
                    ds["Trackhits_found"].loc[BX, run_num, FT] = t_fnd
                    ds["Trackhits_total"].loc[BX, run_num, FT] = t_tot
                    ds["Clusters_found"].loc[BX, run_num, FT] = c_fnd
                    ds["Clusters_total"].loc[BX, run_num, FT] = c_tot




        ds.to_netcdf(args.Output, mode, group=algo)
        eprint(ds)
        eprint("Saved Dataset to {}/{}".format(args.Output, algo))
