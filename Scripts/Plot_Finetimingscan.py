#!/usr/bin/env python
"""
Tool to plot the efficiency finetiming scan
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
import TBFunctions as TB
import numpy as np
import argparse
import matplotlib.pyplot as plt
import danplotlib as dpl
import re
from itertools import izip, cycle
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors

plt.style.use("presentation")
gc_colors.update_colorcycle()

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def extract_layer(file):
    m = re.search("Layer_\d", file)
    if m:
        return m.group(0)
    else:
        raise ValueError("Could not extract Layer from file {}".format(file))

def calc_efficiency_ratio(Eff_up, Eff_down):
    #Eff_X is a np.ndarray with Eff in first column, low err in second column,
    #and up err in third column
    ratio = Eff_up[:,0] / Eff_down[:,0]
    #Add relative errors in quadratic way
    #Note that ratio gets bigger/smaller when Eff_down gets smaller/bigger!
    ratio_lowerr = ratio * np.sqrt( (Eff_up[:,1]/Eff_up[:,0])**2 +
                                    (Eff_down[:,2]/Eff_down[:,0])**2 )
    ratio_uperr = ratio * np.sqrt( (Eff_up[:,2]/Eff_up[:,0])**2 +
                                    (Eff_down[:,1]/Eff_down[:,0])**2 )

    return ratio, ratio_lowerr, ratio_uperr



#Misc settings
algos = ["Default"]
columns = ["BXing", "Finetiming", "TrackHit_Ch"]
BXs = [1, 2]  #Which BXs to show the efficiency for
#BXs = [1]
BX_Ratios = [ [2,1] ] #For which BXs to show the efficiency ratio: First/Second
#BX_Ratios = [ ]
#Which channels to use for calculating the efficiency
#eff_channels = { "Layer_0" : range(5, 45),  #51 Dead
#                 "Layer_1" : range(5, 20) + range(30, 55) }   #22,26 short
FTwindow = 1
FTshift = -2
FTselection = [12+FTshift, 16+FTshift]

#Common eff channels
chrange = range(3,20) + range(29, 48)
eff_channels = { "Layer_0" : chrange, "Layer_1" : chrange }


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot efficiency finetiming scan.")
    parser.add_argument("PACIFICData", help="File to process")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")


    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.PACIFICData))[0]
                     +"_Finetimingscan.pdf" )

    BXsel = " or ".join(["BXing == {}".format(BX) for BX in BXs])
    eprint("Using selection: {}".format(BXsel))
    DF = TB.HDF2DF(args.PACIFICData, clusteralgos=algos, columns=columns,
                   selection=BXsel)
    eprint("{}: Read {} events".format(args.PACIFICData, len(DF)))

    layer_str = extract_layer(args.PACIFICData)
    eff_channels = eff_channels[layer_str]
    eprint("{}: Using channels {} for calculating the efficiency."
           "".format(layer_str, eff_channels))


    with PdfPages(args.save) as pdf:
        for algo in algos:
            eprint("Cluster algorithm: {}".format(algo))
            fig, axes = dpl.subplots(len(BXs), sharex=True)
            if not hasattr(axes, "__iter__"):
                axes = [axes]
            colors = cycle(["gcblue", "gcgreen", "gcred", "gcorange"])
            Effs_dict = {}
            FTs_dict = {}

            #Generate efficiency plots
            for BX, axis in izip(BXs, axes):
                eprint("BXing {}".format(BX))
                DF_BX = DF[ DF["BXing"] == BX ]
                FT_arr = (DF_BX["Finetiming"]+FTshift)%32
                FTs = []
                Effs = []
                for FT in range(0, 32-FTwindow+1):
                    DF_FT = DF_BX[ (FT_arr >= FT) & (FT_arr < FT + FTwindow) ]
                    trackhits = DF_FT["TrackHit_Ch"]
                    clusters = DF_FT["Clusters_{}".format(algo)]
                    eff, _, pur, _, _ = TB.cluster_efficiencies(
                        trackhits, clusters, channel_range=eff_channels)

                    FTs.append(FT+0.5*(FTwindow-1))
                    Effs.append(eff)

                Effs = np.asarray(Effs)
                imax, imin = np.argmax(Effs[:,0]), np.argmin(Effs[:,0])

                color = colors.next()
                axis.errorbar(FTs, Effs[:,0], xerr=FTwindow/2,
                              yerr=[Effs[:,1], Effs[:,2]], color=color,
                              label="BX {}".format(BX))
                #axis.axvline(FTs[imin], ls="--", color=color, label="Min")
                #axis.axvline(FTs[imax], ls="-", color=color, label="Max")
                axis.axvspan(FTselection[0]-0.5, FTselection[-1]+0.5,
                             color="gcgrey", alpha=0.13, linewidth=0,
                             label="FTselection")
                axis.set_xlim(-0.5,31.5)
                axis.autoscale_nooutliers()
                axis.legend(fontsize=18, frameon=True, framealpha=1,
                            edgecolor="none")

                #Activate grid
                axis.grid(which="both")

                #Append data to dicts
                Effs_dict[BX] = Effs
                FTs_dict[BX] = FTs

            axes[0].set_title("Cluster algorithm: {}, FT shift: {}"
                              "".format(algo, FTshift))
            axes[-1].set_xlabel("Arrival time [$780\,\mathrm{ps}$]")
            axes[0].set_ylabel("Efficiency")
            fig.subplots_adjust(hspace=0)
            pdf.savefig(fig)  #Save efficiency plot
            plt.close()


            #Generate efficiency ratio plots
            for BX_up, BX_down in BX_Ratios:
                eprint("Ratio BX{} / BX{}".format(BX_up, BX_down))
                ratio = calc_efficiency_ratio(Effs_dict[BX_up],
                                                         Effs_dict[BX_down])
                FTs = FTs_dict[BX_up]
                FTs_check = FTs_dict[BX_down]
                if FTs != FTs_check:
                    raise ValueError("FTs does not match!")

                dpl.errorbar(FTs, ratio[0], xerr=FTwindow/2,
                             yerr=[ratio[1], ratio[2]],
                             label="BX{} / BX{}".format(BX_up, BX_down))
                dpl.xlabel("Arrival time [$780\,\mathrm{ps}$]")
                dpl.ylabel("Efficiency ratio")
                plt.title("Cluster algorithm: {}, FT shift: {}"
                          "".format(algo, FTshift))
                plt.xlim(-0.5,31.5)
                dpl.autoscale_nooutliers()
                plt.legend(fontsize=18, frameon=True, framealpha=1,
                           edgecolor="none")

                #Acitvate grid
                plt.grid(which="both")



                pdf.savefig()
                plt.close()


    eprint("DONE. Saved plots to {}.".format(args.save))
