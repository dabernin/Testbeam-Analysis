#!/usr/bin/env python
"""
Tool to plot the efficiency (and related stuff) for given data files
(typically PACIFIC and SPIROC for comparison reasons)
"""
from __future__ import print_function, division

import os, sys
sys.path.append("../") #Make TBFunctions accessible
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True  #Let ROOT ignore command args
import TBFunctions as TB
import numpy as np
import pandas as pd
import argparse
import re
import matplotlib.pyplot as plt
import danplotlib as dpl
import itertools as iter
from matplotlib.backends.backend_pdf import PdfPages
import gc_colors
from scipy.optimize import curve_fit

plt.style.use("presentation")
gc_colors.update_colorcycle()


#TUNE THESE PARAMETERS
#=====================

clusteralgorithms = ["Default"]
columns_PACIFIC = ["TrackHit_Ch", "BXing", "Integrator"]
columns_SPIROC = ["TrackHit_Ch"]


selection = "Finetiming >= 12 and Finetiming <= 16"


#Which channels to use for calculating the efficiency
#eff_channels = { "Layer_0" : range(5, 45),  #51 Dead
#                 "Layer_1" : range(5, 20) + range(30, 55) }   #22,26 short

#Common eff channels
chrange = range(3,20) + range(29, 48)
eff_channels = { "Layer_0" : chrange, "Layer_1" : chrange }

#Fit function
def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

#FItting
def fitGaussian(x, y, p0=None):
    #Starting values
    if not p0:
        p0 = [ np.max(y), np.mean(x), np.std(x) ]

    #Perform and plot fit
    coeff, var_matrix = curve_fit(gauss, x, y, p0=p0)

    x_out = np.linspace(np.min(x), np.max(x), 1000)
    y_out = gauss(x_out, *coeff)

    return x_out, y_out, coeff, var_matrix


def extract_layer(file):
    m = re.search("Layer_\d", file)
    if m:
        return m.group(0)
    else:
        raise ValueError("Could not extract Layer from file {}".format(file))

#PLOT FUNCTIONS
#==============
def plot_efficiencies(DFs_PACIFIC, DFs_SPIROC, clusteralgorithm="Default",
                      plotrange=range(64), calcrange=range(5, 45),
                      plot_BXs=None, DF_results=None, evenoddsep=False,
                      ASIC=False):
    markers = iter.cycle(["x", "o", "^", "*", "+", "."])
    fig, axes = dpl.subplots(2, figsize=(22,22))
    #Loop through PACIFIC,SPIROC
    for chip, DFs in enumerate([DFs_PACIFIC, DFs_SPIROC]):
        #Loop through data
        for DF in DFs:
            #Additionally loop through bunch crossings for PACIFIC
            if chip == 0:
                BXs = np.unique(DF["BXing"])
                if plot_BXs:
                    BXs = [ BX for BX in BXs if BX in plot_BXs ]
                print("PACIFIC: Loop through BXs {}".format(BXs))
                evenodd_loop = ["Even", "Odd", None] if evenoddsep else [None]
            else:
                BXs = [ None ] #Dummy for SPIROC
                evenodd_loop = [None]
            for BX in BXs:
                for evenodd in evenodd_loop:
                    DF_BX = DF[ DF["BXing"] == BX ] if BX else DF
                    if evenodd is not None:
                        Integrator = DF_BX["Integrator"]
                        if evenodd == "Even":
                            DF_BX = DF_BX[ Integrator == 0 ]
                        elif evenodd == "Odd":
                            DF_BX = DF_BX[ Integrator == 1 ]

                    trackhits = DF_BX["TrackHit_Ch"]
                    clusters = DF_BX["Clusters_{}".format(clusteralgorithm)]

                    #For plotting per channel
                    chrange = plotrange if chip==0 else [ch+320 for ch in
                                                         plotrange]
                    toteff, cheff, totpur, chpur, chs = TB.cluster_efficiencies(
                        trackhits, clusters, channel_range=chrange)


                    #For calculating mean values in reasonable range
                    chrange = calcrange if chip==0 else [ch+320 for ch in
                                                         calcrange]
                    meaneff, _, meanpur, _, _ = TB.cluster_efficiencies(
                        trackhits, clusters, channel_range=chrange)
                    meaneff, meanpur = np.asarray(meaneff), np.asarray(meanpur)


                    #Labels
                    if chip == 0:
                        label = "BX{}".format(BX)
                        if BX == -1:
                            label += " (max)"
                        if BX == -2:
                            label += " (sum)"
                        if evenodd is not None:
                            label += " ({})".format(evenodd)
                    else:
                        label = "SPIROC"

                    eff = ( r"$\bar{{\epsilon}} = ({:.2f}\pm{:.2f})\,\%$"
                           .format(meaneff[0]*100, np.mean(meaneff[1:])*100) )
                    label_eff = label + "\n" + eff
                    pur = ( r"$\bar{{p}} = ({:.2f}\pm{:.2f})\,\%$"
                           .format(meanpur[0]*100, np.mean(meanpur[1:])*100) )
                    label_pur = label + "\n" + pur

                    #Plotting efficiency
                    if chip == 1:
                        chs = [ch-320 for ch in chs]
                    marker = markers.next()
                    if ASIC:
                        chs = TB.SiPM2ASIC(np.asarray(chs))
                    axes[0].errorbar(chs, cheff[0], xerr=0.5, yerr=cheff[1:],
                                     fmt=marker, label=label_eff, ms=12)
                    #Plotting purity
                    axes[1].errorbar(chs, chpur[0], xerr=0.5, yerr=chpur[1:],
                                     fmt=marker, label=label_pur, ms=12)

                    #Save efficiencies (?)
                    if DF_results is not None:
                        collabel = label.replace(" ", "_")
                        collabel = collabel.replace("(", "").replace(")", "")
                        colname = "{}_{}".format(clusteralgorithm, collabel)
                        DF_results[colname] = cheff[0]
                        DF_results[colname+"_lowerr"] = cheff[1]
                        DF_results[colname+"_uperr"] = cheff[2]


    # GENERAL PLOTTING ========
    for idx, axis in enumerate(axes):
        #Area indicating calcrange
        if ASIC:
            calcrange = TB.SiPM2ASIC(np.asarray(calcrange))
        for calcch in calcrange:
            axis.axvspan(calcch-0.5, calcch+0.5,
                         color="gcgrey", alpha=0.13, linewidth=0)

        axis.autoscale_nooutliers()
        axis.set_xlabel("Channel")
        axis.set_ylabel("Efficiency" if idx == 0 else "Purity")
        axis.legend(fontsize=22, bbox_to_anchor=(1.01, 1), loc=2,
                    borderaxespad=0., labelspacing=1, edgecolor="k",
                    fancybox=False, frameon=True, framealpha=0.13,
                    facecolor="gcgrey")
        axis.set_xlim(plotrange[0]-0.5, plotrange[-1]+0.5)

    title = "{} Clusteralgorithm, {} Channel Ordering".format(
        clusteralgorithm, "ASIC" if ASIC else "SiPM")
    axes[0].set_title(title, fontsize=24)

    return fig, axes








def plot_clustersizes(DFs_PACIFIC, DFs_SPIROC, clusteralgorithm="Default",
                      logy=False, plot_BXs=None, inchannels=range(63)):
    fig, axis = dpl.subplots()
    xmin = 0
    xmax = 16 if logy else 6

    styles = iter.cycle(["-", "--", ":"])
    #Loop through PACIFIC,SPIROC
    for chip, DFs in enumerate([DFs_PACIFIC, DFs_SPIROC]):
        #Loop through data
        for DF in DFs:
            #Additionally loop through bunch crossings for PACIFIC
            if chip == 0:
                BXs = np.unique(DF["BXing"])
                if plot_BXs:
                    BXs = [ BX for BX in BXs if BX in plot_BXs ]
                print("PACIFIC: Loop through BXs {}".format(BXs))
            else:
                BXs = [ None ] #Dummy for SPIROC
                inchannels = [ ch + 320 for ch in inchannels ]
            for BX in BXs:
                DF_BX = DF[ DF["BXing"] == BX ] if BX else DF
                nevents = len(DF_BX)

                cluster_sizes = [ cluster.Size() for evt_clusters in
                                 DF_BX["Clusters_{}".format(clusteralgorithm)]
                                 for cluster in evt_clusters
                                 if np.rint(cluster.Position()+1e-10).astype(
                                     int) in inchannels ]

                #Labels
                if chip == 0:
                    label = "BX{}".format(BX)
                    if BX == -1:
                        label += " (max)"
                    if BX == -2:
                        label += " (sum)"
                else:
                    label = "SPIROC"

                axis.hist(cluster_sizes, weights=[1./nevents]*len(cluster_sizes),
                         bins=np.arange(xmin-0.5, xmax+1.5+1, 1) , label=label,
                         log=logy, ls=styles.next())


    # GENERAL PLOTTING ========
    axis.set_ylabel(r"Clusters / n$_\mathrm{events}$")
    axis.set_xlabel("Cluster size [Channels]")
    axis.legend(fontsize=22)
    title = "{} Clusteralgorithm".format(clusteralgorithm)
    if logy:
        title+=", Logarithmic scale"
    axis.set_title(title, fontsize=24)
    axis.set_xlim(xmin-0.5, xmax+0.5)

    return fig, axis



def plot_correlations(DFs_PACIFIC, DFs_SPIROC, clusteralgorithm="Default",
                      plot_BXs=None):
    out_figs = []
    out_figs_res = []

    #Loop through PACIFIC,SPIROC
    for chip, DFs in enumerate([DFs_PACIFIC, DFs_SPIROC]):
        #Loop through data
        for DF in DFs:
            #Additionally loop through bunch crossings for PACIFIC
            if chip == 0:
                BXs = np.unique(DF["BXing"])
                if plot_BXs:
                    BXs = [ BX for BX in BXs if BX in plot_BXs ]
                print("PACIFIC: Loop through BXs {}".format(BXs))
            else:
                BXs = [ None ] #Dummy for SPIROC
            for BX in BXs:
                #Create figure for residual plot
                fig_res, axis_res = dpl.subplots()

                DF_BX = DF[ DF["BXing"] == BX ] if BX else DF

                #Label
                if chip == 0:
                    label = "BX{}".format(BX)
                    if BX == -1:
                        label += " (max)"
                    if BX == -2:
                        label += " (sum)"
                else:
                    label = "SPIROC"

                trackhits, clusters = TB.cluster_correlation(
                    DF_BX["TrackHit_Ch"],
                    DF_BX["Clusters_{}".format(clusteralgorithm)],
                    closest=True)
                trackhits = np.asarray(trackhits)
                clusters = np.asarray(clusters)
                corr = np.corrcoef(trackhits, clusters)[0,1]

                #Hexbin plot
                fig, axis = dpl.subplots()
                lowlim = 0 if chip == 0 else 320
                uplim = 63 if chip == 0 else 383
                hb = axis.hexbin(clusters, trackhits, gridsize=70, bins="log")
                axis.plot([lowlim, uplim], [lowlim, uplim], color="gcred",
                          ls="--", lw=1)
                axis.set_xlim(lowlim, uplim)
                axis.set_ylim(lowlim, uplim)
                cb = fig.colorbar(hb, ax=axis)
                cb.set_label('log$_{10}$(Entries)')
                axis.set_xlabel("Cluster position [Channel]")
                axis.set_ylabel("Track position [Channel]")
                axis.set_title("{}, Cluster algorithm {}\nCorr.: {:.2%}"
                               "".format(label, clusteralgorithm, corr))
                out_figs.append(fig)


                #Residual plot = Histogram of difference
                diff = trackhits-clusters
                #Convert diff (channel) to micrometer
                diff *= 250
                #Selection
                diff = diff[(diff > -1000) & (diff < 1000)]
                y, xedges, _ = axis_res.hist(
                    diff, bins=np.linspace(-1000,1000,100),
                    histtype="stepfilled")

                x = np.asarray((xedges[:-1] + xedges[1:]))/2
                #Fit gaussian
                xgauss, ygauss, coeff, var_matrix = fitGaussian(x, y)
                axis_res.plot(xgauss, ygauss, ls="--",
                              label="$\sigma = {:.0f}\,\mathrm{{\mu m}}$"
                                    "".format(coeff[2]))

                #Residual plot settings
                axis_res.legend(fontsize=18)
                axis_res.set_ylabel("Entries")
                axis_res.set_xlabel("Residual [$\mathrm{\mu m}$]")
                axis_res.set_title("Cluster algorithm {}, Residual plot, {}"
                                   "".format(clusteralgorithm, label))

                out_figs_res.append(fig_res)


    return out_figs+out_figs_res


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot efficiency")
    parser.add_argument("files", nargs="+", help="Files to process")
    parser.add_argument("--save", "-s", help="Where to save plots.",
                        default="")
    parser.add_argument("--efficiencies", "-e", help="Where to save "
                        "efficiencies as .csv", default="")
    parser.add_argument("--BXs", "-b", help="BXs to plot (only relevant for "
                        "PACIFIC data).", nargs="+", type=int, default=None)
    parser.add_argument("--evenodd", help="Divide PACIFIC data into even and "
                        "odd timestamps/integrators", action="store_true")



    args = parser.parse_args()
    if not args.save:
        args.save = ( os.path.splitext(os.path.basename(args.file))[0]
                      +"_Efficiency.pdf" )


    DFs_PACIFIC = []
    DFs_SPIROC = []
    Layers = []
    for File in args.files:
        Layers.append(extract_layer(File))
        if "PACIFIC" in File:
            DF = TB.HDF2DF(File, clusteralgorithms, BXs=args.BXs,
                           selection=selection)
            print("Read PACIFIC file {} using selection {} ({} events/rows)."
                  .format(File, selection, len(DF)))

            DFs_PACIFIC.append(DF)
        elif "SPIROC" in File:
            DF = TB.HDF2DF(File, clusteralgorithms)
            print("Read SPIROC file {} ({} events)."
                  .format(File, len(DF)))
            DFs_SPIROC.append(DF)

    Layer = Layers[0]
    if len(np.unique(Layers)) != 1:
        print("WARNING: Different Layers found ({})".format(Layers))
        print("Using the first one in list.. ({})".format(Layer))
    else:
        print("{} detected (Use channels {} to calculate the mean efficiency)."
              "".format(Layer, eff_channels[Layer]))


with PdfPages(args.save) as pdf:
    print("Plotting cluster-track correlations...")
    for algo in clusteralgorithms:
        print("Clusteralgorithm: {}".format(algo))
        figs = plot_correlations(DFs_PACIFIC, DFs_SPIROC, algo,
                                 plot_BXs=args.BXs)

        for fig in figs:
            pdf.savefig(fig)
            plt.close(fig)

    print("Plotting efficiencies...")
    DF_results = pd.DataFrame(index=range(64))  #DF to store efficiencies
    DF_results.index.name = "Ch"
    DF_results["Ch_ASIC"] = TB.SiPM2ASIC(DF_results.index.values)
    for algo in clusteralgorithms:
        print("Clusteralgorithm: {}".format(algo))
        for ASIC in [False]:
            print("ASIC Ordering" if ASIC else "SiPM Ordering")
            fig, axes = plot_efficiencies(DFs_PACIFIC, DFs_SPIROC, algo,
                                          plot_BXs=args.BXs,
                                          calcrange=eff_channels[Layer],
                                          DF_results=DF_results,
                                          evenoddsep=args.evenodd,
                                          ASIC=ASIC)
            pdf.savefig(fig)
            plt.close(fig)

    if args.efficiencies:
        DF_results.to_csv(args.efficiencies, sep="\t", float_format="%.5f")
        print("Saved efficiencies to {}..".format(args.efficiencies))

    print("Plotting cluster sizes...")
    for algo in clusteralgorithms:
        print("Clusteralgorithm: {}".format(algo))
        for logy in [True]:
            print("Linear scale" if not logy else "Logarithmic scale")
            fig, axis = plot_clustersizes(DFs_PACIFIC, DFs_SPIROC, algo, logy,
                                          plot_BXs=args.BXs,
                                          inchannels=eff_channels[Layer])
            pdf.savefig(fig)
            plt.close(fig)


print("DONE. Saved plots to {}.".format(args.save))
