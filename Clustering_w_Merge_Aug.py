from __future__ import print_function, division

from Clusteralgorithm import cluster_algorithm, Cluster
import numpy as np
import root_numpy as rn
import root_pandas as rp   #For reading in chunks
from rootpy.io import root_open  #For getting number of entries
import pandas as pd
from argparse import ArgumentParser
import TBFunctions as TB
import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


#Chunksize
chunksize = int(2e6)

#Channels to use for the clustering
channels = range(0, 64, 1)
SPIROC_channels = range(320, 384, 1)

#Channels to keep in the SPIROC data
#They will be overwritten with the threshold data
#The pixel-data will be accessible at Ch_Pixels_{number}
SPIROC_keep_channels = ["Ch_{}".format(ch) for ch in range(320, 384)]
SPIROC_pixel_channels = [ col.replace("Ch_", "Ch_Pixels_") for col in
                          SPIROC_keep_channels ]

#Data_columns for on-disk queries
#PACIFIC_data_columns = [ "Layer", "BXing", "Integrator", "Finetiming", "TrackHit_Ch",
#                         "Track_Chi2NDOF", "TelescopeClusters", "TrackHit_Y" ]
PACIFIC_data_columns = [ "Integrator", "Finetiming" ]
#SPIROC_data_columns = [ "Layer", "TrackHit_Ch", "Track_Chi2NDOF", "TelescopeClusters",
#                        "TrackHit_Y" ]
SPIROC_data_columns = None

#Possible cluster algorithm settings to choose from
cluster_algo_settings = { "Default" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3},
                          #"No_requirements" : {"seed_th" : 2, "min_size" : 0, "single_ch_th" : 0},
                          #"Seed_1" : {"seed_th" : 1, "min_size" : 0, "single_ch_th" : 0},
                          #"Big" : {"seed_th" : 2, "min_size" : 2, "single_ch_th" : 3, "continue_th" : 1},
                        }



if __name__ == "__main__":
    #Read options
    #Create optionparser
    parser = ArgumentParser(usage="Tool to create clusters and merge into data")

    parser.add_argument( "-i", "--input", dest="Input", action="store", required=True,
                         help="Input ROOT-file")
    parser.add_argument( "-t", "--tree", dest="Tree", action="store", required=False,
                        choices=["PACIFIC", "SPIROC"], default="PACIFIC")
    parser.add_argument( "-o", "--output", dest="Output", action="store", required=True,
                         help="Output HDF5-file")
    parser.add_argument( "-s", "--thsetting", dest="ThSetting", required=False,
                        help="Threshold setting (SPIROC only, e.g. 152535)")
    #parser.add_argument( "-n", "--nevents", dest="Nevents", action="store", required=False,
    #                    type=int, help="Only process this amount of events.")


    #Parse arguments from command line
    options = parser.parse_args()


    #==============================
    # PACIFIC ======================
    #==============================
    if options.Tree == "PACIFIC":
        print("PACIFIC data provided")

        DF_chunks = rp.read_root(options.Input, options.Tree,
                                 chunksize=chunksize)

        data_columns = PACIFIC_data_columns
        cluster_channels = channels     #Build clusters within these channels

        #Define categories for later saving
        all_columns = rn.list_branches(options.Input, options.Tree)
        categories = { "ChData" : [],
                       "Data" : [] }
        for col in all_columns:
            if col.startswith("Ch_"):
                categories["ChData"].append(col)
            else:
                categories["Data"].append(col)




    #==============================
    # SPIROC ======================
    #==============================
    if options.Tree == "SPIROC":
        print("SPIROC data provided")

        #Check if -s is provided when -t is provided
        if not options.ThSetting:
            raise ValueError("Processing SPIROC data but no threshold setting was set!")

        elif len(options.ThSetting) != 6:
            raise ValueError("ThSetting must be a 6 digit number (e.g. 152535 for Ths 1.5 2.5 3.5)")

        else:
            Th1 = float(options.ThSetting[0]+"."+options.ThSetting[1])
            Th2 = float(options.ThSetting[2]+"."+options.ThSetting[3])
            Th3 = float(options.ThSetting[4]+"."+options.ThSetting[5])

            SPIROC_ThSetting = [ Th1, Th2, Th3 ]

        #Get columns from root-file to read
        all_columns = rn.list_branches(options.Input, options.Tree)
        columns = []  #Only read these columns
        for col in all_columns:  #Keep only specific Ch_ columns
            if "Ch_" not in col or col in SPIROC_keep_channels:
                columns.append(col)


        print("Read columns {} from {}, Tree {}".format(columns, options.Input, options.Tree))
        DF_chunks = rp.read_root(options.Input, options.Tree, columns=columns,
                                 chunksize=chunksize)


        #Define categories for later saving
        categories = { "ChData" : SPIROC_keep_channels,  #ChThData (as for PACIFIC)
                       "Data" : [],   #Other data
                       "ADCData" : [],
                       "PixelData" : SPIROC_pixel_channels }
        for col in columns:
            if col.startswith("ADC_Ch_"):
               categories["ADCData"].append(col)
            else:
                if not col.startswith("Ch_"):
                    categories["Data"].append(col)



        data_columns = SPIROC_data_columns
        cluster_channels = SPIROC_channels




    #=========
    #COMMON
    #=========
    #Number of entries/rows
    with root_open(options.Input) as f:
        tree = f.Get(options.Tree)
        n_entries = tree.GetEntries()
    print("{} events/rows available in the provided file".format(n_entries))
    n_chunks = int(np.ceil(n_entries/chunksize))

    #Print categories
    print("Will save to categories", categories)

    #Create container to hold ALL clusters for each BX and clusteralgo
    clusters_dict = {}

    #Loop through chunks and save data
    filecreated  = False
    for n_chunk, DF in enumerate(DF_chunks):
        print("Processing chunk {} of {}...".format(n_chunk+1, n_chunks))
        print("Read {} events/rows.".format(len(DF)))
        print("Indices: {}".format(DF.index.values))
        unique_BXings = np.unique(DF["BXing"]) if "BXing" in DF.columns else [ None ]
        print("Found BXings: ", unique_BXings)
        for n_BX, BX in enumerate(unique_BXings):
            if BX is not None:
                print("Processing BXing {}...".format(BX))
                DF_BX = DF[ DF["BXing"] == BX ]
                path = "BX{}/".format(BX)
            else:
                #Transform SPIROC pixels into exceeded thresholds
                ThS = options.ThSetting
                print("Transforming channel pixels in thresholds ThS{} ({})"
                      .format(ThS, SPIROC_ThSetting))
                DF[ SPIROC_pixel_channels ] = DF[ SPIROC_keep_channels ]
                DF[ SPIROC_keep_channels ] = TB.SPIROC2Ths( DF[ SPIROC_keep_channels ].values,
                                                            SPIROC_ThSetting )
                DF_BX = DF  #Dummy for SPIROC
                path = ""

            #Append to existing dataframes only for n_chunk != 0
            if n_chunk == 0:
                append = False
            else:
                append = True

            #Save individual categories
            for cat, cols in categories.iteritems():
                if not cols:
                    continue
                catpath = path+cat
                DF_cat = DF_BX[cols]

                print("Saving {} to {}/{}...".format(cat, options.Output, catpath))

                if cat == "Data":  #Data columns only make sense for Data cat
                    dc = data_columns
                else:
                    dc = None

                if filecreated:
                    mode = "a"  #Append if file already created
                else:
                    mode = "w"  #(Re)create file otherwise
                    filecreated = True
                DF_cat.to_hdf(options.Output, catpath, format="table", mode=mode, complevel=1,
                          complib="zlib", data_columns=dc, append=append)


            print("Performing clustering")
            chmatrix = DF_BX[ ["Ch_{}".format(ch) for ch in cluster_channels] ].values
            index = DF_BX.index
            for name, settings in cluster_algo_settings.iteritems():
                print(name, settings)
                DF_Clusters = pd.DataFrame(index=index)
                key = "Clusters_{}".format(name)
                DF_Clusters[key] = [ cluster_algorithm( charray, channel_range=cluster_channels,
                                                       **settings) for charray in chmatrix ]

                #Save into cluster container/dictionary
                clusterkey = path+key
                if clusterkey in clusters_dict:
                    #Append if already exists
                    clusters_dict[clusterkey] = clusters_dict[clusterkey].append(
                        DF_Clusters, verify_integrity=True)
                else:
                    #Create new if not exists
                    clusters_dict[clusterkey] = DF_Clusters


    #Loop through chunks finished, now save clusters stored in clusters_dict
    for key, clusters in clusters_dict.iteritems():
        #Save to HDF (every cluster configuration in a different key (performance reasons))
        print("Saving Clusters to {} ({} events)".format(key, len(clusters)))
        if filecreated:
            mode = "a"  #Append if file already created
        else:
            mode = "w"  #(Re)create file otherwise
            filecreated = True
        clusters.to_hdf(options.Output, key, format="fixed", mode=mode,
                           complevel=1, complib="zlib")
